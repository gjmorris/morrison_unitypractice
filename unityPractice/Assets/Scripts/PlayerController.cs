﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public Text countText;
    public Text winText;
    bool temp = false;
    int count = 0;
    public GameObject pickup;
    public Transform cameraTransform;

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody>();
        setCountText();
        spawnPickups();
        

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Space))
        {
            temp = true;
        }
        else
        {
            temp = false;
        }

	}

    void FixedUpdate() {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 camForward = cameraTransform.forward;
        Vector3 camRight = cameraTransform.right;

        camForward = new Vector3(camForward.x, 0, camForward.z);
        camForward = camForward.normalized;

        Vector3 movement = (camForward * moveVertical) + (camRight * moveHorizontal);//new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(100.0f * movement);

        if (temp)
        {
            rb.AddForce(new Vector3(0, 500.0f, 0));
        }

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Pickup"))
        {
            count++;
            Destroy(other.gameObject);
            setCountText();
            setWinText();
        }

    }

    void setCountText()
    {
        countText.text = "Count: " + count;
    }

    void setWinText()
    {
        if (count >= 12)
        {
            winText.text = "YOU WIN!";
        }
    }


    void spawnPickups()
    {
        for (int i = 0; i < 20; i++)
        {

            float x = Random.Range(-8f, 8f);
            float y = Random.Range(0.5f, 95f);
            float z = Random.Range(-8, 8f);

            Instantiate(pickup, new Vector3(x, y, z), Quaternion.identity);
        }
    }
}
