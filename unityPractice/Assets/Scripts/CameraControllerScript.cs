﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerScript : MonoBehaviour {

    public GameObject player;
    public GameObject cameraAnchor;
    Vector3 offset;
    public float turnSpeed = 4f;

	// Use this for initialization
	void Start () {

        offset = transform.position - player.transform.position;

	}
	
	// Update is called once per frame
	void Update () {


	}

    private void LateUpdate()
    {

        //transform.position = player.transform.position + offset;

        offset = Quaternion.AngleAxis(Input.GetAxis("Fire1") * turnSpeed, Vector3.up) * offset;
        transform.position = player.transform.position + offset;
        transform.LookAt(player.transform.position);

        

    }
}
